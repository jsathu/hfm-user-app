import React from 'react';
import { Icon } from 'native-base';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as firebase from 'firebase';

import HomeScreen from './src/screens/HomeScreen.js';
import RequestSongScreen from './src/screens/RequestSongScreen.js';
import ProgramsScreen from './src/screens/ProgramsScreen.js';

const Tab = createBottomTabNavigator();

const firebaseConfig = {
  apiKey: "AIzaSyAT6SlQF2EH3YQx6rsA4xfl6vqFQTpYXZM",
  authDomain: "hfm-hcc.firebaseapp.com",
  databaseURL: "https://hfm-hcc.firebaseio.com",
  projectId: "hfm-hcc",
  storageBucket: "hfm-hcc.appspot.com",
  messagingSenderId: "842435382557",
  appId: "1:842435382557:web:61a06c0eb51c766d87e5f4",
  measurementId: "G-BZBRVNEBNB"
};

firebase.initializeApp(firebaseConfig);

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ color, size }) => {
              let iconName;
              if (route.name === "Home") {
                iconName = "home";
              }
              else if (route.name === "Request Song") {
                iconName = "music";
              }
              else {
                iconName = "list";
              }
              return <Icon name={iconName} type="FontAwesome" style={{color:"white"}}/>
            }
          })}
          tabBarOptions={{
        //    activeBackgroundColor: "#A4B0BD",
            activeTintColor: "#74B9FF",
            style:{
              backgroundColor:"#000000",
              borderTopColor:"black"
            }
          }}>
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="Request Song" component={RequestSongScreen} />
          <Tab.Screen name="Program List" component={ProgramsScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    )
  }
}

