import React from 'react';
import { StyleSheet, Text, SafeAreaView, View, Platform } from 'react-native';
import { Icon } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as Font from 'expo-font';
import Loading from '../component/Loading.js';
import * as Linking from 'expo-linking';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class DetailsScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isFontLoaded: false
    }
  }

  loadFont = async () => {
    await Font.loadAsync({
      ralewayBold: require("../fonts/Raleway-Bold.ttf"),
      nunitoRegular: require("../fonts/nunito-sans.regular.ttf"),
      ralewayMedium: require("../fonts/Raleway-Medium.ttf")
    })
    this.setState({ isFontLoaded: true })
  }

  openAppstore = () => {
    if (Platform.OS == "android") {
      Linking.openURL('http://play.google.com/store/apps/details?id=com.google.android.apps.maps');
    } else {
      Linking.openURL('itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=643496868&amp;onlyLatestVersion=true&amp;pageNumber=0&amp;sortOrdering=1&amp;type=Purple+Software');
    }
  }

  componentDidMount() {
    this.loadFont();
  }

  render() {
    if (this.state.isFontLoaded) {
      return (
        <SafeAreaView style={styles.container}>
          <Text style={styles.text}>HFM</Text>
          <View style={styles.paragraph}>
            <Text style={styles.paraText}>
              This app is developed by ict union of HinduCollege Colombo 04  HFM is a project by 2022 Prefects Guild...
              proud to be a hinduite !!!! kari aathal full atthal vera lvl aathal edungaaaaaaaa fun thamami...... {'\n'}
              - 2022 ICT UNION - fun ahh
          </Text>
          </View>
          <TouchableOpacity onPress={() => { this.openAppstore() }}>
            <Text style={styles.linkText}>Rate us ...</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate("Report") }}>
            <View style={{
              flexDirection: "row", marginTop: hp("4%"), alignSelf: "center"
            }}>
              <Text style={styles.linkBug}> Bug Report  </Text>
              <Icon name="bug" type="FontAwesome" style={{ color: "#ffffff" }} />
            </View>
          </TouchableOpacity>
        </SafeAreaView>
      )
    } else {
      return (
        <Loading />
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000'
  },
  text: {
    fontSize: hp("4%"),
    fontFamily: "ralewayMedium",
    marginTop: hp("3%"),
    marginRight: wp("30%"),
    color: "#ffffff",
  },
  paragraph: {
    margin: wp("6%")
  },
  paraText: {
    fontSize: hp("3%"),
    fontFamily: "nunitoRegular",
    color: "#ffffff",
  },
  linkText: {
    fontSize: hp("3%"),
    fontFamily: "ralewayBold",
    color: "#0A79DF",
    marginTop: hp("10%"),
    alignSelf: "center"
  },
  linkBug: {
    fontSize: hp("3%"),
    fontFamily: "ralewayBold",
    color: "#0A79DF",
  }
});
