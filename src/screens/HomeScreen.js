import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './Home';
import ReportScreen from './ReportScreen.js';
import DetailsScreen from './DetailsScreen.js';

const Stack = createStackNavigator();

export default class HomeScreen extends React.Component {
    render() {
        return (
            <Stack.Navigator initialRouteName="Home" screenOptions={{headerShown:false}}>
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="Report" component={ReportScreen} />
                <Stack.Screen name="Details" component={DetailsScreen} />
            </Stack.Navigator>
        )
    }
}

