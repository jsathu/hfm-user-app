import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, Alert, SafeAreaView, Platform, Animated, Easing, ScrollView } from 'react-native';
import { Icon } from 'native-base';
import * as Network from 'expo-network';
import * as firebase from 'firebase';
import { Audio } from 'expo-av';
import * as Font from 'expo-font';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Loading from '../component/Loading.js';

const soundObject = new Audio.Sound();

export default class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isPlaying: false,
            status: "play-circle",
            isLoaded: false,
            networkStatus: false,
            appStatus: false,
            url: "",
            onAir: {},
            isFontLoaded: false,
            spinning: false
        }
        this._rotationAnimation = new Animated.Value(0);
        //   this._rotationOffset = 0;
    }

    playFm = async () => {
        if (this.state.isPlaying !== true) {
            if (this.state.isLoaded !== false) {
                await soundObject.playAsync()
                    .then(() => {
                        this.setState({ status: "pause-circle", isPlaying: true });
                        this.toggleSpinning();
                    })
                    .catch((error) => {
                        console.log(error);
                    })
            } else {
                alert('Error Occured');
            }
        } else {
            await soundObject.pauseAsync()
                .then(() => {
                    this.setState({ status: "play-circle", isPlaying: false });
                    this.toggleSpinning();
                })
                .catch((error) => {
                    console.log(error);
                })
        }
    }

    getNetworkStatus = async () => {
        await Network.getNetworkStateAsync()
            .then((status) => {
                this.setState({ networkStatus: status.isInternetReachable })
                this.getAppStatus();
                this.loadUrl();
                this.getOnAir();
            })
            .catch((error) => console.log(error));
    }

    loadSound = async () => {
        if (this.state.url !== "") {
            await soundObject.loadAsync({ uri: this.state.url })
                .then(() => {
                    this.setState({ isLoaded: true });
                })
                .catch((error) => {
                    this.setState({ isLoaded: false });
                    console.log(error);
                })
        } else {
            alert("Please restart the app....");
        }

    }

    loadUrl = () => {
        let ref = firebase.database().ref("url")
        ref.once("value", (url) => {
            if (url.val()) {
                this.setState({ url: url.val() })
            } else {
                alert("Error occured !!!!");
            }
        })
            .then(() => {
                this.loadSound();
            })
            .catch((error) => console.log(error))
    }

    loadFont = async () => {
        await Font.loadAsync({
            ralewayBold: require("../fonts/Raleway-Bold.ttf"),
            robotoMedium: require("../fonts/Roboto-Medium.ttf"),
            nunitoRegular: require("../fonts/nunito-sans.regular.ttf"),
            nunito: require("../fonts/nunito-sans.semibold.ttf"),
            ralewayMedium: require("../fonts/Raleway-Medium.ttf")
        })
        this.setState({ isFontLoaded: true })
    }

    getOnAir = () => {
        let reference = firebase.database().ref("onAir")
        reference.on("value", (data) => {
            if (data.val()) {
                let result = data.val()
                this.setState({ onAir: result })
            } else {
                let defaultOnAir = {
                    title: "Not Available...",
                    host: "......",
                    imgUrl: "https://iim.cmb.ac.lk/wp-content/uploads/2019/12/not-available.jpg"
                }
                this.setState({ onAir: defaultOnAir })
            }
        })
    }

    setAudioOptions = async () => {
        await Audio.setAudioModeAsync({
            playsInSilentModeIOS: true,
            staysActiveInBackground: true,
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DUCK_OTHERS,
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DUCK_OTHERS,
            shouldDuckAndroid: false,
            playThroughEarpieceAndroid: false,
        })
            .catch((error) => console.log(error))
    }

    appOff = () => {
        alert("Broadcast is currently off now... ");
    }

    getAppStatus = () => {
        let ref = firebase.database().ref("status");
        ref.on("value", (data) => {
            if (data.val() === "true") {
                this.setState({ appStatus: true });
            } else {
                this.setState({ appStatus: false });
            }
        })
    }

    loadAnimation = () => {
        //  this._rotationAnimation.setOffset(this._rotationOffset);
        this._rotationAnimation.setValue(0);
        Animated.loop(
            Animated.timing(this._rotationAnimation, {
                toValue: 1,
                duration: 2200,
                easing: Easing.linear,
                useNativeDriver: true
            })
        ).start();
        //  this.forceUpdate();
    }

    stopLoopAnimation = () => {
        this._rotationAnimation.stopAnimation();
        //  this.forceUpdate();
    };

    toggleSpinning = () => {
        this.setState({ spinning: !this.state.spinning }, () => {
            this.state.spinning
                ? this.loadAnimation()
                : this.stopLoopAnimation();
        });
    }

    getRotationAnimation = () => {
        const rotate = this._rotationAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: ["0deg", "360deg"]
        }); return { rotate };
    };

    componentDidMount() {
        this.loadFont();
        this.getNetworkStatus();
        this.setAudioOptions();
    }


    render() {
        if (this.state.networkStatus !== false) {
            if (this.state.appStatus !== false) {
                return (
                    <SafeAreaView style={styles.container}>
                        <ScrollView>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate("Details") }}>
                                <Icon name="info-circle" type="FontAwesome" style={styles.details} />
                            </TouchableOpacity>

                            <Text style={styles.topText}>NOW ON AIR...</Text>
                            <View style={styles.onAirStyle}>
                                <Image style={styles.onAirImage} source={{ uri: this.state.onAir.imgUrl }} />
                                <Text style={styles.onAirText}>{this.state.onAir.title}{'\n'}{this.state.onAir.host}</Text>
                            </View>

                            <Animated.Image
                                style={{
                                    transform: [this.getRotationAnimation()],
                                    width: wp("100%"),
                                    height: hp("45%"),
                                    alignSelf: "center",
                                }}
                                source={require("../images/mainLogo.png")} />

                            <TouchableOpacity onPress={() => { this.playFm() }}>
                                <Icon name={this.state.status} type="FontAwesome" style={styles.musicButton} />
                            </TouchableOpacity>
                        </ScrollView>
                    </SafeAreaView>
                )
            } else {
                return (
                    <SafeAreaView style={styles.container}>
                        <ScrollView>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate("Details") }}>
                                <Icon name="info-circle" type="FontAwesome" style={styles.details} />
                            </TouchableOpacity>

                            <Image
                                style={styles.mainImage}
                                source={require("../images/mainLogo.png")} />

                            <TouchableOpacity onPress={() => { this.appOff() }}>
                                <Icon name={this.state.status} type="FontAwesome" style={styles.musicButton} />
                            </TouchableOpacity>
                        </ScrollView>
                    </SafeAreaView>
                )
            }
        } else {
            return (
                <Loading />
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000000'
    },
    onAirStyle: {
        borderRadius: hp("2%"),
        //  borderColor: "#30336B",
        //  alignItems: "center",
        // justifyContent: "center",
        flexDirection: "row",
        // marginTop: hp("8%"),
        //   borderWidth: hp("1%"),
        width: wp("80%"),
        height: hp("11%"),
        // margin: hp("8%"),
        alignSelf: "center",
        //  borderWidth:wp("0.5%"),
        // borderColor:"#ffffff",
        backgroundColor: "#4C4B4B"
    },
    onAirText: {
        fontSize: hp("2.2%"),
        fontFamily: "nunito",
        //  alignSelf:"center",
        marginTop: hp("2%"),
        marginRight: wp("30%"),
        color: "#ffffff",
    },
    onAirImage: {
        width: wp("20%"),
        height: hp("8%"),
        // marginRight: hp("8%"),
        // margin: hp("1.8%"),
        marginTop: hp("1.5%"),
        marginHorizontal: wp("2%")
    },
    topText: {
        //  marginTop: hp("1%"),
        fontSize: hp("2.8%"),
        fontFamily: "nunitoRegular",
        alignSelf: "center",
        color: "#ffffff",
        fontWeight: "bold"
    },
    img: {
        width: wp("40%"),
        height: hp("40%"),
        alignSelf: "center",
        justifyContent: "center"
    },
    musicButton: {
        fontSize: wp("35%"),
        color: "white",
        //  marginTop: hp("1%"),
        alignSelf: "center",
        // justifyContent:"center",
        alignItems: "center"
    },
    details: {
        fontSize: wp("8%"),
        color: "#7B8788",
        alignSelf: "flex-end",
        //alignItems: "flex-end",
        marginTop: hp("2%"),

    },
    mainImage: {
        width: wp("100%"),
        height: hp("45%"),
        alignSelf: "center",
        // justifyContent: "center",
    },
});
