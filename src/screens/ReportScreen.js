import React from 'react';
import { StyleSheet, Text, View, Linking, Alert, Vibration, SafeAreaView, TouchableWithoutFeedback, Keyboard } from 'react-native';
import * as Font from 'expo-font';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Input, Button, Item } from 'native-base';
import Loading from '../component/Loading.js';

export default class ReportScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      issue: "",
      isFontLoaded: false
    }
  }

  loadFont = async () => {
    await Font.loadAsync({
      ralewayMedium: require("../fonts/Raleway-Medium.ttf")
    })
    this.setState({ isFontLoaded: true })
  }

  componentDidMount() {
    this.loadFont();
  }

  sendIssue = () => {
    let newIssue = this.state.issue;
    if (this.state.issue !== "") {
      Linking.canOpenURL(`mailto:jsathurshan7@gmail.com?subject=Issue Reported&body=${newIssue}`)
        .then((supported) => {
          if (supported) {
            this.setState({ issue: "" });
            return Linking.openURL(`mailto:jsathurshan7@gmail.com?subject=Issue Reported&body=${newIssue}`);
          } else {
            alert("Issue cannot be reported !!!");
          }
        })
    } else {
      alert("Type issue");
      Vibration.vibrate(500);
    }
  }

  render() {
    if (this.state.isFontLoaded) {
      return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <SafeAreaView style={styles.container}>
            <Text style={styles.text}>Report Issue</Text>
            <Item rounded style={styles.item}>
              <Input value={this.state.issue} onChangeText={(issue) => { this.setState({ issue }) }} style={{ color: "#ffffff", fontFamily: "ralewayMedium" }} />
            </Item>
            <Button block onPress={() => { this.sendIssue() }} style={styles.button}><Text style={styles.buttonText}>Report</Text></Button>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      )
    } else {
      return (
        <Loading />
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000',
  },
  text: {
    fontSize: hp("3%"),
    fontFamily: "ralewayMedium",
    marginTop: hp("3%"),
    marginRight: wp("32%"),
    color: "#ffffff",
  },
  item: {
    width: wp("90%"),
    height: hp("7%"),
    marginTop: hp("10%"),
    alignSelf: "center"
  },
  button: {
    width: wp("25%"),
    height: hp("6%"),
    borderRadius: hp("1.3%"),
    backgroundColor: "#ffffff",
    alignSelf: "center",
    margin: wp("10%"),
  },
  buttonText: {
    fontSize: hp("3%"),
    fontFamily: "nunito",
    color: "#000000"
  }
});
