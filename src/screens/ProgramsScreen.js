import React from 'react';
import { StyleSheet, Text, SafeAreaView, FlatList, Alert, ScrollView, RefreshControl, View, Image, Vibration, Platform, TouchableOpacity, ActivityIndicator } from 'react-native';
import { CardItem } from 'native-base';
import * as Permissions from 'expo-permissions';
import * as firebase from 'firebase';
import * as Font from 'expo-font';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Notifications } from 'expo';

export default class ProgramsScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      programs: [],
      isFontLoaded: false,
      refreshing: false,
      isNotificationSet: false
    }
  }

  getPrograms = () => {
    let ref = firebase.database().ref('programs');
    ref.on("value", (dataSnapShot) => {
      if (dataSnapShot.val()) {
        let programs = Object.values(dataSnapShot.val());
        this.setState({ programs });
      } else {
        alert("Check your internet connection and restart the app...");
      }
    })
    this.setState({ refreshing: false });
  }

  setNotification = (time, title, host) => {

    let localNotification = {
      title: title,
      body: `hosted by ${host}`,
      ios: {
        sound: true
      },
      android: {
        sound: true,
        color: "blue"
      },
      vibrate: true
    }

    let result = time.split(':');
    let hours = parseInt(result[0]);
    let minutes = parseInt(result[1]);

    let notificationTime = new Date();
    // notification should be scheduled by admin everyday morning or after 12 AM
    notificationTime.setHours(hours);
    notificationTime.setMinutes(minutes);
    notificationTime.setSeconds(0);

    let currentTime = new Date();

    if (currentTime.getTime() >= notificationTime.getTime()) {
      alert("The program has already finished !!!");
      Vibration.vibrate(400);
    } else {
      let schedulingOptions = {
        time: notificationTime
      }
      Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions)
        .then(() => { alert("Notification has been set") })
        .catch((error) => console.log(error))
    }
  }


  askPermissionForNotification = async () => {
    let result = (await Permissions.askAsync(Permissions.NOTIFICATIONS)).status;
    console.log(result);
    if (result.status !== "granted") {
      alert("Please allow to send notifications !!!");
    }
  }

  loadFont = async () => {
    await Font.loadAsync({
      ralewayBold: require("../fonts/Raleway-Bold.ttf"),
      robotoMedium: require("../fonts/Roboto-Medium.ttf"),
      nunitoRegular: require("../fonts/nunito-sans.regular.ttf"),
      nunito: require("../fonts/nunito-sans.semibold.ttf"),
      ralewayMedium: require("../fonts/Raleway-Medium.ttf")
    })
    this.setState({ isFontLoaded: true })
  }

  componentDidMount() {
    this.loadFont();
    this.getPrograms();
    this.askPermissionForNotification();
  }

  render() {
    if (this.state.programs.length !== 0) {
      return (
        <SafeAreaView style={styles.container}>
          <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => { this.getPrograms() }} />}>
            <Text style={styles.text}>Programs</Text>
            <FlatList
              data={this.state.programs}
              renderItem={({ item }) => {
                return (
                  <CardItem button style={styles.card} onPress={() => { this.setNotification(item.time, item.title, item.host) }}>
                    <Image source={{ uri: `${item.imgUrl}` }} style={styles.image} />
                    <View style={styles.textView}>
                      <Text style={styles.listTextOne}>{item.title}</Text>
                      <Text style={styles.listTextTwo}>{item.host}</Text>
                      <Text style={styles.listTextThree}>{item.time}</Text>
                    </View>
                  </CardItem>
                )
              }}
              keyExtractor={(item) => item.time} />
            <Text style={styles.textLast}>Press the card to get notified</Text>
          </ScrollView>
        </SafeAreaView>
      )
    } else {
      return (
        <SafeAreaView style={styles.containerTwo}>
          <ActivityIndicator size="large" />
          <Text style={styles.textNoPrograms}>Please Wait....</Text>
        </SafeAreaView>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000',
  },
  containerTwo: {
    flex: 1,
    backgroundColor: '#000000',
    alignContent: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: hp("3%"),
    fontFamily: "ralewayMedium",
    marginTop: hp("3%"),
    marginRight: wp("32%"),
    color: "#ffffff",
  },
  listTextOne: {
    fontSize: hp("3%"),
    fontFamily: "robotoMedium",
    color: "#000000",
  },
  listTextTwo: {
    fontSize: hp("2%"),
    fontFamily: "ralewayMedium",
    color: "#ffffff",
  },
  listTextThree: {
    fontSize: hp("2%"),
    fontFamily: "nunito",
    color: "#ffffff",
  },
  card: {
    backgroundColor: "#7B8788",
    width: wp("95%"),
    alignSelf: "center",
    marginTop: hp("5%"),
    flexDirection: "row",
    height: hp("14%"),
    borderRadius: wp("1.5%")
  },
  listButton: {
    width: wp("16%"),
    height: hp("5%"),
    borderRadius: hp("1.3%"),
    backgroundColor: "#ffffff",
  },
  buttonText: {
    fontSize: hp("3%"),
    fontFamily: "nunito",
    color: "#000000",
  },
  image: {
    width: wp("25%"),
    height: hp("12%"),
    alignSelf: "center"
  },
  textView: {
    marginLeft: wp("4%"),
    alignSelf: "center"
  },
  buttonView: {
    marginBottom: hp("0.5%"),
    marginTop: hp("0.5%"),
    marginRight: wp("10%")
  },
  textLast: {
    fontSize: hp("2%"),
    fontFamily: "nunitoRegular",
    marginTop: hp("4%"),
    color: "#ffffff",
    alignSelf: "center"
  },
  textNoPrograms: {
    fontSize: hp("3%"),
    fontFamily: "nunitoRegular",
    color: "#ffffff",
    alignSelf: "center"
  },
});
