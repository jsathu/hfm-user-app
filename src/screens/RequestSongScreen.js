import React from 'react';
import { StyleSheet, Text, SafeAreaView, Vibration, Alert, View, Keyboard, TouchableWithoutFeedback, Platform } from 'react-native';
import { Input, Button, Item, Container, Content, Form, Label } from 'native-base';
import * as Font from 'expo-font';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as firebase from 'firebase';
import Loading from '../component/Loading.js';

export default class RequestSongScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      song: "",
      artist: "",
      dedicatedTo: "",
      isFontLoaded: false
    }
  }

  sendRequest = () => {

    if (this.state.song !== "") {
      if (this.state.artist !== "") {
        let details = {
          songName: this.state.song,
          artistName: this.state.artist,
          dedicated: this.state.dedicatedTo
        }
        if (details.dedicated === "") {
          details.dedicated = "Didn't mention it";
        }
        // console.log(details.dedicated);
        firebase.database().ref('requestedSongs/').push(details)
          .then(() => {
            this.setState({
              song: "",
              artist: "",
              dedicatedTo: ""
            })
            alert("Requested Successfuly !!!!");
            this.props.navigation.navigate("Home");
          })
          .catch((error) => console.log(error));
      } else {
        alert("Type an artist");
        Vibration.vibrate(400);
      }
    } else {
      alert("Type a song");
      Vibration.vibrate(400);
    }
  }

  loadFont = async () => {
    await Font.loadAsync({
      nunitoRegular: require("../fonts/nunito-sans.regular.ttf"),
      nunito: require("../fonts/nunito-sans.semibold.ttf"),
      ralewayMedium: require("../fonts/Raleway-Medium.ttf")
    })
    this.setState({ isFontLoaded: true })
  }

  componentDidMount() {
    this.loadFont();
  }

  render() {
    if (this.state.isFontLoaded) {
      return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <SafeAreaView style={styles.container}>
            <Text style={styles.text}>Request Song</Text>
            <View style={styles.input}>
              <Form>
                <Item inlineLabel>
                  <Label>Song</Label>
                  <Input value={this.state.song} style={{ color: "#ffffff" }} onChangeText={(song) => { this.setState({ song }) }} />
                </Item>
                <Item inlineLabel>
                  <Label>Artist</Label>
                  <Input value={this.state.artist} style={{ color: "#ffffff" }} onChangeText={(artist) => { this.setState({ artist }) }} />
                </Item>
                <Item inlineLabel>
                  <Label>Dedicated to</Label>
                  <Input value={this.state.dedicatedTo} style={{ color: "#ffffff" }} onChangeText={(dedicatedTo) => { this.setState({ dedicatedTo }) }} />
                </Item>
              </Form>
            </View>
            <Button block onPress={() => { this.sendRequest() }} style={styles.button}><Text style={styles.buttonText}>Request</Text></Button>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      )
    } else {
      return (
        <Loading />
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000'
  },
  text: {
    fontSize: hp("3%"),
    fontFamily: "ralewayMedium",
    //  alignSelf:"center",
    marginTop: hp("3%"),
    marginLeft: wp("8%"),
    color: "#ffffff",
  },
  buttonText: {
    fontSize: hp("3%"),
    fontFamily: "nunitoRegular",
  },
  button: {
    width: wp("35%"),
    height: hp("6%"),
    marginTop: hp("10%"),
    alignSelf: "center",
    backgroundColor: "#2F4F4F",
    borderRadius: hp("2.5%")
  },
  input: {
    marginTop: hp("15%"),
    fontSize: hp("5%"),
    fontFamily: "nunito",
    alignSelf: "center",
    color: "#ffffff",
    width: wp("90%"),
    justifyContent: "center"
  },
});