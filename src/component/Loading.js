import React from 'react';
import { StyleSheet, Text, View ,ActivityIndicator} from 'react-native';

export default class Loading extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Loading... Check your network!!</Text>
        <ActivityIndicator size="large"  style={{marginBottom:20}}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
